This is a hack to BookStack, using the theme system, so that you can configure notifications to be sent to users within roles defined via tags applied to parent books.
For example, if a tag with name `Notify` and value `Admins, Viewers` is applied to a book, updates to pages within will be notified via email to all users within the "Admins" and "Viewers" roles.

### Setup

This uses the [logical theme system](https://github.com/BookStackApp/BookStack/blob/development/dev/docs/logical-theme-system.md).

1. Within the BookStack install folder, you should have a `themes` folder.
2. Create a `themes/custom/functions.php` file with the contents of the `functions.php` file example below.
3. Customize the email message, if desired, by editing the lines of text within the `toMail` part at around lines 36-39.
4. Add `APP_THEME=custom` to your .env file.

### Note

The sending of emails may slow down page update actions, and these could be noisy if a user edits a page many times quickly. 
You may run into email system rate-limits with the amount of emails being sent.
These customizations are not officially supported any may break upon, or conflict with, future updates. 
Quickly tested on BookStack v22.11.